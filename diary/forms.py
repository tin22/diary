from django import forms
from django.contrib.admin import widgets


class SearchForm(forms.Form):
    query = forms.CharField(max_length=100)

#
# class TodoForm(forms.Form):
#     action = forms.CharField(max_length=255)
#     deadline = forms.DateTimeField(widget=widgets.AdminSplitDateTime)
