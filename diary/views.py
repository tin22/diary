from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.shortcuts import get_object_or_404, render
from django.views.generic import TemplateView, ListView, DetailView
from django.views.generic.edit import CreateView
from taggit.models import Tag

from diary.forms import SearchForm  # TodoForm
from diary.models import Entry, Counterparty, Document, Todo


class HomepageView(TemplateView):
    template_name = 'diary/home.html'


class EntryListView(LoginRequiredMixin, ListView):
    queryset = Entry.objects.order_by('-timestamp')
    paginate_by = 10
    context_object_name = 'entry_list'
    template_name = 'diary/entry_list.html'


class EntryDetailView(LoginRequiredMixin, DetailView):
    model = Entry
    template_name = 'diary/entry_detail.html'


class EntryCreateView(LoginRequiredMixin, CreateView):
    model = Entry
    template_name = 'diary/entry_new.html'
    fields = ('timestamp', 'record', 'organization', 'documents', 'tags',)


class DocumentListView(LoginRequiredMixin,ListView):
    queryset = Document.objects.all()
    template_name = 'diary/document_list.html'
    context_object_name = 'document_list'


class DocumentDetailView(LoginRequiredMixin, DetailView):
    model = Document

    # template_name = 'diary/document_detail.html'

    def get_template_names(self):
        return 'diary/document_detail.html'


class DocumentCreateView(LoginRequiredMixin, CreateView):
    model = Document
    template_name = 'diary/document_new.html'
    fields = ('type', 'number', 'date', 'organization', 'items', 'amount',)


class CounterpartyListView(LoginRequiredMixin, ListView):
    model = Counterparty


class CounterpartyDetailView(LoginRequiredMixin, DetailView):
    model = Counterparty
    template_name = 'diary/counterparty_detail.html'


class CounterpartyCreateView(LoginRequiredMixin, CreateView):
    model = Counterparty
    template_name = 'diary/counterparty_new.html'
    fields = ('name', 'full_name', 'phone', 'person', 'address', 'note',)


class TodoListView(LoginRequiredMixin, ListView):
    model = Todo
    context_object_name = 'todos'
    template_name = 'diary/todo_list.html'


class TodoDetailView(LoginRequiredMixin, DetailView):
    model = Todo
    template_name = 'diary/todo_detail.html'


class TodoCreateView(LoginRequiredMixin, CreateView):
    model = Todo
    # form_class = TodoForm
    fields = ('action', 'deadline', 'note')
    template_name = 'diary/todo_new.html'


def entry_list(request, tag_slug=None):
    object_list = Entry.objects.all()
    tag = None
    if tag_slug:
        tag = get_object_or_404(Tag, slug=tag_slug)
        object_list = object_list.filter(tags__in=[tag])
    paginator = Paginator(object_list, 10)
    page = request.GET.get('page')
    try:
        entries = paginator.page(page)
    except PageNotAnInteger:
        # Если указанная страница не является целым числом.
        entries = paginator.page(1)
    except EmptyPage:
        # Если указанный номер больше, чем всего страниц, возвращаем последнюю.
        entries = paginator.page(paginator.num_pages)
    return render(request, 'diary/list_by_tag.html', {'page': page, 'entries': entries, 'tag': tag})


def entry_search(request):
    form = SearchForm()
    query = None
    results = []
    if 'query' in request.GET:
        form = SearchForm(request.GET)
        if form.is_valid():
            query = form.cleaned_data['query']
            results = Entry.objects.filter(record__icontains=query)
    return render(request,
                  'diary/entry_search.html',
                  {'form': form,
                   'query': query,
                   'results': results})


# def endlog():
#     logger.info('==== Program ended. ==== \n \n')
#
#
# logger.info('==== Program started ====')
# atexit.register(endlog)
