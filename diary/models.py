from django.db import models
from django.urls import reverse
from django.utils import timezone

from loguru import logger
from taggit.managers import TaggableManager


class Entry(models.Model):
    """ Daily entries
    """
    timestamp = models.DateTimeField(verbose_name='Отметка времени ', default=timezone.now)
    record = models.TextField('Запись ', blank=False, null=False)
    organization = models.ForeignKey('Counterparty', verbose_name='Организация ', on_delete=models.DO_NOTHING,
                                     related_name='entries',
                                     blank=True, null=True)
    documents = models.ManyToManyField('Document', verbose_name='Документы ', blank=True, related_name='entries',
                                       help_text=' Отметьте все связанные документы.')
    tags = TaggableManager(blank=True)

    class Meta:
        db_table = 'entries'
        verbose_name = 'Запись'
        ordering = ('-timestamp',)
        verbose_name_plural = 'Записи'

    def __str__(self):
        return f"{self.timestamp.strftime('%d.%m.%Y %H:%M')} {self.record}"

    def get_absolute_url(self):
        return reverse('entry_detail', args=[str(self.pk)])


class Document(models.Model):
    """ Available documents
    """
    DOC_TYPES = (
        ('B', 'Счет'),  # Bill
        ('C', 'Договор'),  # Contract
        ('R', 'Квитанция'),  # Receipt
        ('V', 'Доверенность'),  # Vicarious authority
        ('L', 'Товарная накладная'),  # Packing list
        ('I', 'Счет фактура'),  # Invoice
        ('A', 'Акт выполненных работ'),  # Act of completion
        ('P', 'Документ'),  # Paper
        ('O', 'Приказ'),  # Order
        ('E', 'Письмо'),  # lEtter
    )
    type = models.CharField(max_length=1, choices=DOC_TYPES)
    number = models.CharField(max_length=20)  # Number of document
    date = models.DateField()
    organization = models.ForeignKey('Counterparty', on_delete=models.DO_NOTHING, related_name='documents')
    items = models.TextField()  # Subject of document
    amount = models.DecimalField(max_digits=8, decimal_places=2, blank=True, null=True)

    class Meta:
        db_table = 'documents'
        ordering = ('-date',)
        verbose_name = 'Документ'
        verbose_name_plural = 'Документы'

    def __str__(self):
        return f"{self.get_type_display()} №{self.number} от {self.date}"

    def get_absolute_url(self):
        return reverse('document_detail', args=[str(self.pk)])


class Counterparty(models.Model):
    """ Counterparties info
    """
    name = models.CharField(max_length=50, default='Horns and hooves')  # Brief name
    full_name = models.TextField(blank=True)
    phone = models.CharField(max_length=64, default='unknown')
    person = models.CharField(max_length=64, blank=True, default='')
    address = models.CharField(max_length=128, default='unknown')
    note = models.TextField(blank=True)

    class Meta:
        db_table = 'counterparties'
        verbose_name = 'Контрагент'
        verbose_name_plural = 'Контрагенты'

    def __str__(self):
        return f"{self.name}"

    def get_absolute_url(self):
        return reverse('counterparty_detail', args=[str(self.pk)])


class Todo(models.Model):
    """ Todo List Items
    """
    STATUSES = (
        ('A', 'Ожидает выполнения'),  # Active
        ('C', 'Выполнено'),  # Complete
        ('I', 'Неактуально'),  # Irrelevant
        ('D', 'Отложено'),  # Deferred
        ('E', 'Просрочено'),  # Expired
    )
    action = models.CharField(max_length=255)
    deadline = models.DateTimeField(blank=True, null=True)
    status = models.CharField(max_length=1, choices=STATUSES)
    note = models.TextField(blank=True)

    class Meta:
        db_table = 'todo'
        verbose_name = 'Выполнить'
        verbose_name_plural = 'Выполнить'

    def __str__(self):
        """ Show & change status if expired
        """
        if self.deadline and self.status == 'A' and self.deadline <= timezone.now():
            self.status = 'E'
            logger.info(f"Auto expired: {self.deadline <= timezone.now()} (deadline: {self.deadline}, "
                        f"now: {timezone.now()}) Status changed.")
            self.save()
        return f"{self.deadline} {self.action} - {self.status}"

    def get_absolute_url(self):
        return reverse('todo_detail', args=[str(self.pk)])
