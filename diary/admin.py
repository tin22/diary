from django.contrib import admin

from .models import Entry, Document, Counterparty, Todo


@admin.register(Entry)
class EntryAdmin(admin.ModelAdmin):
    list_display = ('timestamp', 'record',)
    ordering = ['-timestamp', ]
    # list_filter = ('tags',)
    search_fields = ('record',)
    date_hierarchy = 'timestamp'


@admin.register(Document)
class DocumentAdmin(admin.ModelAdmin):
    list_display = (str, 'items', 'amount')
    pass


@admin.register(Counterparty)
class CounterpartyAdmin(admin.ModelAdmin):
    list_display = ('name', 'full_name', 'phone', 'person')


@admin.register(Todo)
class TodoAdmin(admin.ModelAdmin):
    pass
